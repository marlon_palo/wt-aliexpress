package com.scrape.item;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.xml.xpath.XPathExpressionException;

import org.openqa.selenium.WebDriver;
import org.w3c.dom.Document;

import com.grund.form.ScrollIntoView;
import com.grund.request.ClickElement;
import com.grund.store.StoreLinkParameter;
import com.grund.utility.DownloadImage;
import com.grund.utility.StatusLog;
import com.grund.utility.XMLElements;
import com.grund.verify.VerifyDocumentURL;
import com.grund.verify.verifyXPath;

public class item {

	private static final String SPAN_LOWPRICE_XPATH = "//span[@itemprop='lowPrice']";
	private static final String SPAN_HIGHPRICE_XPATH = "//span[@itemprop='highPrice']";
	private static final String SPAN_PRICE_XPATH = "//span[@id='j-sku-price']";
	private static final String SPAN_ORDERSTEXT_XPATH = "//span[@id='j-order-num']";
	private static final String SPAN_VOTESTEXT_XPATH = "//span[@class='rantings-num']";
	private static final String SPAN_RATINGSTEXT_XPATH = "//span[@class='percent-num']";
	private static final String IMG_ITEMIMAGE_XPATH = "//span[@class='img-thumb-item']//img";
	private static final String IMG_MAGNIFIEDIMAGE_XPATH = "//div[@id='magnifier']//img";
	private static final String H1_PRODUCTNAME_XPATH = "//h1[@itemprop]";
	private static final String LINK_STORENAME_XPATH = "//a[@class='store-lnk']";
	private static final String DT_PROPERTYITEMTITLE_XPATH = "//div[@id='j-product-attribute-main']//dt";
	private static final String SPAN_PRODUCTDETAILSFIELD_XPATH = "//span[@class='propery-title']";
	private static final String SPAN_PRODUCTDETAILSFIELDVALUE_XPATH = "//span[@class='propery-des']";
	private static final String TEXT_STOCKQTY_XPATH = "//em[@id='j-sell-stock-num']";
	private static final String LINK_SHIPPINGPAYMENTTAB_XPATH = "//a[text()='Shipping & Payment']";
	private static final String TD_SHIPPINGCOMPANY_XPATH = "//td[@class='col-company']";
	private static final String SPAN_SHIPPINGCOST_XPATH = "//span[@class='s-price']";
	private static final String SPAN_SHIPPINGESTDELIVERYTIME_XPATH = "//span[@class='s-time']";
	private static final String COL_SHIPPINGCOST_XPATH = "//td[@class='col-price']";
	private static final String SPAN_SHIPPINGUNITTYPE_XPATH = "(//li[@class='packaging-item']//span[text()='Unit Type:']) [position()=2]/../span[@class='packaging-des']";
	

	public static String getLowPriceText(WebDriver driver) {
		
		return verifyXPath.getText(driver, SPAN_LOWPRICE_XPATH, "ALI EXPRESS - PDP: Get the Lowest Price");
	}
	
	public static boolean isItemLowPriceAvail(WebDriver driver) {
		
		try {
			int xpathctr = verifyXPath.count(driver, SPAN_LOWPRICE_XPATH);	
			if(xpathctr > 0){
				return true;
			} else {
				return false;
			}
			
		} catch (Exception e) {
			return false;
		}
	}
	
	public static String getHighPriceText(WebDriver driver) {
		return verifyXPath.getText(driver, SPAN_HIGHPRICE_XPATH, "ALI EXPRESS - PDP: Get the Highest Price");
	}
	
	public static boolean isItemHighPriceAvail(WebDriver driver) {
		try {
			int xpathctr = verifyXPath.count(driver, SPAN_HIGHPRICE_XPATH);	
			if(xpathctr > 0){
				return true;
			} else {
				return false;
			}
			
		} catch (Exception e) {
			return false;
		}
	}

	

	public static String getPrice(WebDriver driver) {
		return verifyXPath.getText(driver, SPAN_PRICE_XPATH, "ALI EXPRESS - PDP: Get the Price");
	}

	public static boolean verifyURL(WebDriver driver, String urltext) {
		
		try {
			return VerifyDocumentURL.containsText(driver, urltext, "ALI EXPRESS - PDP: Verify the Page is PDP");
		} catch (Exception e) {
			return false;
		}
	}

	public static String getOrdersText(WebDriver driver) {
		return verifyXPath.getText(driver, SPAN_ORDERSTEXT_XPATH, "1", "1", "ALI EXPRESS - PDP: Get the Orders text.");
	}

	public static String getVotesText(WebDriver driver) {
		return verifyXPath.getText(driver, SPAN_VOTESTEXT_XPATH, "1", "1", "ALI EXPRESS - PDP: Get the Votes text.");
	}

	public static String getRatingsValue(WebDriver driver) {
		return verifyXPath.getText(driver, SPAN_RATINGSTEXT_XPATH, "1", "1", "ALI EXPRESS - PDP: Get the Ratings text.");
	}

	public static String getURL(WebDriver driver) throws MalformedURLException {
		// TODO Auto-generated method stub
		return StoreLinkParameter.getCurrentURLQuery(driver, "ALI EXPRESS - PDP: Get the URL Query.");
	}

	public static int getTotalImage(WebDriver driver) {
		
		return verifyXPath.count(driver, IMG_ITEMIMAGE_XPATH, "ALI EXPRESS - PDP: Get Total Image.");
	}

	public static void clickThumbNailImagebyPosition(WebDriver driver, int pos) {
		ClickElement.byXPath(driver, "(" + IMG_ITEMIMAGE_XPATH + ") [position()=" + pos + "]", "ALI EXPRESS - PDP: Click Thumbnail Image in position " + pos);
		
	}

	public static void downLoadMagnifyImage(WebDriver driver, String outputFileDir) {
		try {
			DownloadImage.byXPathSRCAttribute(driver, IMG_MAGNIFIEDIMAGE_XPATH, outputFileDir);
		} catch (Throwable e) {
			StatusLog.log("ERROR ON DOWNLOADING IMAGE TO DIRECTORY: " + outputFileDir);
		}
		
	}

	public static String getProductName(WebDriver driver) {
		return verifyXPath.getText(driver, H1_PRODUCTNAME_XPATH, "1", "1", "ALI EXPRESS - PDP: Get the Product Title.");
	}

	public static String getStoreName(WebDriver driver) {
		return verifyXPath.getText(driver, LINK_STORENAME_XPATH, "1", "1", "ALI EXPRESS - PDP: Get the Store Name.");
	}

	public static String getStoreLink(WebDriver driver) {
		return verifyXPath.getAttributeValue(driver, LINK_STORENAME_XPATH, "href");
	}

	public static int getPropertyItemCount(WebDriver driver) throws Exception {
		return verifyXPath.count(driver, DT_PROPERTYITEMTITLE_XPATH, "2", "1");
	}

	public static String getPropertyLabelTextbyPos(WebDriver driver, int pos) {
		return verifyXPath.getText(driver, "(" + DT_PROPERTYITEMTITLE_XPATH + ") [position()=" + pos + "]", "1", "1", "ALI EXPRESS - PDP: Get the Property Label in position " + pos);
	}

	public static int getPropItemSpanCountbyPos(WebDriver driver, int pos) throws Exception {
		return verifyXPath.count(driver, "(" + DT_PROPERTYITEMTITLE_XPATH + ") [position()=" + pos + "]/..//span", "2", "1");
	}

	public static int getPropItemLinkCountbyPos(WebDriver driver, int pos) throws Exception {
		return verifyXPath.count(driver, "(" + DT_PROPERTYITEMTITLE_XPATH + ") [position()=" + pos + "]/..//a", "2", "1");
	}

	public static String getPropItemSpanbyPosandValuePos(WebDriver driver,
			int pos, int pos2) {
		return verifyXPath.getText(driver, "((" + DT_PROPERTYITEMTITLE_XPATH + ") [position()=" + pos + "]/..//span) [position()=" + pos2 + "]", "1", "1", "ALI EXPRESS - PDP: Get the Property Label position " + pos + " span text value in position " + pos2);
	}

	public static String getPropItemLinkTitlebyPosandValuePos(WebDriver driver,
			int pos, int pos2) {
		return verifyXPath.getAttributeValue(driver, "((" + DT_PROPERTYITEMTITLE_XPATH + ") [position()=" + pos + "]/..//a) [position()=" + pos2 + "]", "title", "ALI EXPRESS - PDP: Get the Property Label position " + pos + " link title value in position " + pos2);
	}

	public static int getProductDetailsFieldCount(WebDriver driver) throws Exception {
		return verifyXPath.count(driver, SPAN_PRODUCTDETAILSFIELD_XPATH, "2", "1", "ALI EXPRESS - PDP: Get the count of Product Details Field.");
	}

	public static String getProductDetailFieldLabelbyPos(WebDriver driver, int pos) {
		return verifyXPath.getText(driver, "(" + SPAN_PRODUCTDETAILSFIELD_XPATH + ") [position()=" + pos + "]", "2", "1", "ALI EXPRESS - PDP: Get the Product Detail Field text.");
	}

	public static String getProductDetailFieldValuesbyPos(WebDriver driver,
			int pos) {
		return verifyXPath.getText(driver, "(" + SPAN_PRODUCTDETAILSFIELDVALUE_XPATH + ") [position()=" + pos + "]", "2", "1", "ALI EXPRESS - PDP: Get the Product Detail Field Value.");
	}

	public static String getStockQtyCount(WebDriver driver) {
		return verifyXPath.getText(driver, TEXT_STOCKQTY_XPATH, "2", "1", "ALI EXPRESS - PDP: Get Quantity Stock count.");
	}

	public static String getMagnifyImageSRCAttribute(WebDriver driver) {
		
		return verifyXPath.getAttributeValue(driver, IMG_MAGNIFIEDIMAGE_XPATH, "src", "ALI - EXPRESS - PDP: Get the Url of Magnified Image..");
	}

	public static void clickShippingPaymentTab(WebDriver driver) {
		try {
			ScrollIntoView.byXPath(driver, LINK_SHIPPINGPAYMENTTAB_XPATH, "ALI - EXPRESS - PDP: Scroll page to view the Shipping tab.");
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		ClickElement.byXPath(driver, LINK_SHIPPINGPAYMENTTAB_XPATH,"ALI - EXPRESS - PDP: Click Shipping & Payment tab.");
		
	}

	public static int getShippingCompanyCount(WebDriver driver) throws InterruptedException {
		return verifyXPath.count(driver, TD_SHIPPINGCOMPANY_XPATH, "2", "1", "ALI - EXPRESS - PDP: Get Shipping Company Count.");
	}

	public static String getShippingCompanybyPosition(WebDriver driver, int pos) {
		return verifyXPath.getText(driver, "(" + TD_SHIPPINGCOMPANY_XPATH + ") [position()=" + pos + "]", "2", "1", "ALI EXPRESS - PDP: Get Shipping Company Name in position. " + pos);
		
	}

	public static String getShippingCostybyPosition(WebDriver driver, int pos) {
		return verifyXPath.getText(driver, "(" + COL_SHIPPINGCOST_XPATH + ") [position()=" + pos + "]//span", "2", "1", "ALI EXPRESS - PDP: Get Shipping Company Name in position. " + pos);
		
	}

	public static String getEstimatedDeliveryTimebyPosition(WebDriver driver,
			int pos) {
		return verifyXPath.getText(driver, "(" + SPAN_SHIPPINGESTDELIVERYTIME_XPATH + ") [position()=" + pos + "]", "2", "1", "ALI EXPRESS - PDP: Get Shipping Company Name in position. " + pos);
		
	}

	public static String getShippingUnitType(WebDriver driver) {
		return verifyXPath.getText(driver, SPAN_SHIPPINGUNITTYPE_XPATH, "2", "1", "ALI EXPRESS - PDP: Get Shipping Unit Type. ");
		
	}

	public static ArrayList<String> getProductNameinHTMLSource(Document document) throws XPathExpressionException {
		
		return XMLElements.getElementTextContentbyXPath(document, "//h1[@itemprop]");
	}

	
}
