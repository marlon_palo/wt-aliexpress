package com.scrape.login;

import org.openqa.selenium.WebDriver;

import com.grund.form.SetInputField;
import com.grund.request.ClickElement;

public class login {

	private static final String INPUT_ACCOUNTFIELD_XPATH = "//input[@id='fm-login-id']";
	private static final String INPUT_PASSWORDFIELD_XPATH = "//input[@id='fm-login-password']";
	private static final String BTN_SIGNIN_XPATH = "//input[@id='fm-login-submit']";

	public static void setAccountField(WebDriver driver, String text) {
		SetInputField.byXPath(driver, INPUT_ACCOUNTFIELD_XPATH, text, "ALI EXPRESS - LOGIN: Set Account: " + text);
		
	}

	public static void setPasswordField(WebDriver driver, String text) {
		SetInputField.byXPath(driver, INPUT_PASSWORDFIELD_XPATH, text, "ALI EXPRESS - LOGIN: Set Password");
		
	}

	public static void clickSignIn(WebDriver driver) throws Exception {
		ClickElement.byXPath(driver, BTN_SIGNIN_XPATH);
		
	}

}
