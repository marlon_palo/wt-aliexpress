package com.scrape.includes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.jsoup.Jsoup;
import org.openqa.selenium.WebDriver;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.grund.engine.Config;
import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;
import com.grund.utility.TakeScreenShot;
import com.grund.utility.Wait;
import com.grund.utility.WindowSwitch;
import com.grund.utility.XMLElements;
import com.scrape.item.item;
import com.scrape.search.Srch;

public class PDP {
	
	static Properties sys = System.getProperties();
	
	
	public static void extractProductDataFromSearchCurrentPage(WebDriver driver, String keyword, int lastPageItemCtr, String excelFileDir) throws Exception {
		
		int pageItemCtr;
		int RowLastNum;		
		String itemLink = "";

		pageItemCtr = Search.getItemListCountOnPage(driver);  //NUMBER OF RESULTS IN A PAGE.
		
		RowLastNum = TableContainer.getRowCountinExcelFile(excelFileDir);
		
		//From search page, get pdp details
		for(int i = 1; i <= pageItemCtr; i++){
			
			itemLink = Srch.getResultItemHrefbyPosition(driver, i);
			
			Srch.clickResultRowbyPosition(driver, i);   //OPEN THE PDP
			Wait.sleep("3");
			WindowSwitch.childWindow(driver);
			
			//BEGIN EXTRACT DATA IN CURRENT PDP PAGE
			PDP.extractProductDataFromCurrentItemPage(Config.driver, i, itemLink, excelFileDir);
			
			WindowSwitch.returntoParentWindow(driver);
			Wait.sleep("3");
			
		} //end for
		
		
	}

	private static boolean isItemDetailPageDisplay(WebDriver driver) {
	
		return item.verifyURL(driver, "https://www.aliexpress.com/item");
	}

	private static String getLowPriceText(WebDriver driver) {
		
		if(item.isItemLowPriceAvail(driver)){
			return item.getLowPriceText(driver);
		} else {
			return item.getPrice(driver);
		}
			
	}
	
	private static String getHighPriceText(WebDriver driver) {
		if(item.isItemHighPriceAvail(driver)){
			return item.getHighPriceText(driver);
		} else {
			return item.getPrice(driver);
		}
	}
	
	private static String getOrdersCount(WebDriver driver) {
		String ordersTxt = "";
		
		ordersTxt = item.getOrdersText(driver);
		ordersTxt = ordersTxt.replaceAll("[^\\d.]", "");
		
		return ordersTxt;
	}
	
	private static String getVotesCount(WebDriver driver) {
		String votesTxt = "";
		
		votesTxt = item.getVotesText(driver);
		votesTxt = votesTxt.replaceAll("[^\\d.]", "");
		
		return votesTxt;
	}

	private static String getRatingsValue(WebDriver driver) {
		return item.getRatingsValue(driver);
	}

	private static String getURL(WebDriver driver) throws MalformedURLException {	
		return item.getURL(driver);
	}
	

	private static String getTotalImage(WebDriver driver) {
		int intTotal;
		String txtTotal;
		
		intTotal = item.getTotalImage(driver);
		txtTotal = String.valueOf(intTotal);
		
		return txtTotal;
		
		
	}
	

	private static void downloadAllImages(WebDriver driver, String outputDir) throws InterruptedException {
		int intTotal = item.getTotalImage(driver);
		
		for(int i = 1; i <= intTotal; i++){
			item.clickThumbNailImagebyPosition(driver, i);
			Wait.sleep("3");
			item.downLoadMagnifyImage(driver, outputDir + i + ".png");
			
		}
		
	}
	
	private static String getandDownloadImageURLbyPosition(WebDriver driver, int pos, String outputDir) throws InterruptedException {
		item.clickThumbNailImagebyPosition(driver, pos);
		Wait.sleep("3");
		item.downLoadMagnifyImage(driver, outputDir + pos + ".png");
		return item.getMagnifyImageSRCAttribute(driver);
	}

	public static void extractProductDataFromCurrentItemPage(WebDriver driver,
			int itemCtr, String itemURL, String excelFileDir) throws Exception {
		
		int RowLastNum;
		int rowIndex;
		String itemLink = "";
		String productName = "";
		String store = "";
		String storeLink = "";
		String shipsFrom = "";
		String price = "";
		String discountPrice = "";
		String orders = "";
		String ratingVotes = "";
		String rating = "";
		String stock = "";
		String imageLink1 = "";
		String imageLink2 = "";
		String imageLink3 = "";
		String imageLink4 = "";
		String imageLink5 = "";
		String imageLink6 = "";
		String shippingType = "";
		
		
		String totalImages = "";
		String outputImageDir = "";

		RowLastNum = TableContainer.getRowCountinExcelFile(excelFileDir);
		rowIndex = RowLastNum + 1;
		
		//Data from PDP
		if(isItemDetailPageDisplay(driver)){
			itemLink = itemURL;
			productName = item.getProductName(driver);
			rating = getRatingsValue(driver);
			ratingVotes = getVotesCount(driver);
			orders = getOrdersCount(driver);
			store = item.getStoreName(driver);
			storeLink = item.getStoreLink(driver);
			price = item.getPrice(driver);
			discountPrice = getLowPriceText(driver) + " - " + getHighPriceText(driver); 
			stock = item.getStockQtyCount(driver);
			outputImageDir = sys.getProperty("downloadDir") + "\\image\\" + itemCtr + "\\";
			imageLink1 = getandDownloadImageURLbyPosition(driver, 1, outputImageDir);
			imageLink2 = getandDownloadImageURLbyPosition(driver, 2, outputImageDir);
			imageLink3 = getandDownloadImageURLbyPosition(driver, 3, outputImageDir);
			imageLink4 = getandDownloadImageURLbyPosition(driver, 4, outputImageDir);
			imageLink5 = getandDownloadImageURLbyPosition(driver, 5, outputImageDir);
			imageLink6 = getandDownloadImageURLbyPosition(driver, 6, outputImageDir);
			shippingType = item.getShippingUnitType(driver);
			totalImages = getTotalImage(driver);
			
			//PDP.downloadAllImages(driver, outputImageDir + itemCtr + "\\");
			
			
		} else{  //the page redirects to a album or promo page

			orders = "0";
			ratingVotes = "0";
			rating = "0";
			itemLink = itemURL;
			totalImages = "";
		}//end if
		
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "ID",String.valueOf(rowIndex));
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "Item Link",itemLink);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "Title",productName);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "Rating",rating);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "Rating Votes",ratingVotes);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "Orders",orders);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "Store",store);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "StoreLink",storeLink);
		
		PDP.insertPropertyItemsIntoCellValue(driver, excelFileDir, rowIndex);
			
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "Price",price);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "Discount Price",discountPrice);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "QtyStock",stock);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "ImageCount",totalImages);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "ImageLink1",imageLink1);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "ImageLink2",imageLink2);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "ImageLink3",imageLink3);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "ImageLink4",imageLink4);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "ImageLink5",imageLink5);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "ImageLink6",imageLink6);
		TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "ShippingType1",shippingType);
		//Product Details tab
		PDP.insertProductDetailsDataIntoCellValue(driver, excelFileDir, rowIndex);
		//Shipping tab
		PDP.insertShippingInfoDataIntoCellValue(driver, excelFileDir, rowIndex);
		
	}

	private static void insertPropertyItemsIntoCellValue(WebDriver driver,
			String excelFileDir, int rowIndex) throws Exception {
		
		int propItemCtr = item.getPropertyItemCount(driver);
		int excelFieldCtr = 0;
		String propLabel = "";
		String propValue = "";
		
		for(int i = 1; i <= propItemCtr; i++){
			
			propLabel = item.getPropertyLabelTextbyPos(driver, i);
			
			if(!propLabel.isEmpty()){  //skip empty label
				
				if(!propLabel.contains("Quantity:")){   //skip Quantity label
					propValue = getPropItemTitleValuesbyPos(driver, i);
					
					excelFieldCtr++; //insert in excel file on next column field: PropertyItemxx
					TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "PropertyItem" + excelFieldCtr,propLabel);
					TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "PropertyItem" + excelFieldCtr + ".Values",propValue);
				}
				
				
			} //end if
			
		} //end for
		
		
	}

	private static String getPropItemTitleValuesbyPos(WebDriver driver, int pos) throws Exception {
		
		String txtHolder = "";
		
		int propValuesSpanCtr = item.getPropItemSpanCountbyPos(driver, pos); //get property item values is in <span>		
		int propValuesLinkCtr = item.getPropItemLinkCountbyPos(driver, pos); //get property item values is in <a>
		
		//if the value text is is in <span>, store all the text in string separate by comma.
		if(propValuesSpanCtr > 0){
			String txtValue;
			for(int s=1; s <= propValuesSpanCtr; s++ ){
				txtValue = item.getPropItemSpanbyPosandValuePos(driver, pos, s); //By the property item position, get the values each by position
				
				if (txtValue.trim().length() > 0){
					if(s == 1){
						txtHolder = txtValue;
					} else {
						txtHolder = txtHolder + "," + txtValue; //the 2nd and succeeding value separate by comma.
					}
				} //end if
				
				
			} //end for
			
			
		} else if(propValuesLinkCtr > 0){
			String txtValue;
			for(int s=1;s <= propValuesLinkCtr;s++ ){
				txtValue = item.getPropItemLinkTitlebyPosandValuePos(driver, pos, s); //By the property item position, get the values each by position
				
				if(s == 1){
					txtHolder = txtValue;
				} else{
					txtHolder = txtHolder + "," + txtValue; //the 2nd and succeeding value separate by comma.
				}
				
			} //end for
		}
		
		
		
		return txtHolder;
	}

	private static void insertProductDetailsDataIntoCellValue(WebDriver driver,
			String excelFileDir, int rowIndex) throws Exception {
		
		int detailFieldCtr = item.getProductDetailsFieldCount(driver);
		String fieldLabel = "";
		String fieldValue = "";
		
		for(int i = 1; i <= detailFieldCtr; i++){
			
			fieldLabel = item.getProductDetailFieldLabelbyPos(driver, i);
	
			if(fieldLabel.trim().length() > 0){
				
				fieldValue = item.getProductDetailFieldValuesbyPos(driver, i);
				TableContainer.insertIntoCellValue(excelFileDir, rowIndex,  "ProductField" + i, fieldLabel);
				TableContainer.insertIntoCellValue(excelFileDir, rowIndex,  "ProductField" + i + ".Value", fieldValue);
				
			} //end if
			
		} //end for
		
	}

	private static void insertShippingInfoDataIntoCellValue(WebDriver driver,
			String excelFileDir, int rowIndex) throws InterruptedException {
		String ShippingCompany = "";
		String ShippingCost = "";
		String EstimatedDeliveryTime = "";
		
		
		item.clickShippingPaymentTab(driver);
		Wait.sleep("3");
		
		int shipCoCtr = item.getShippingCompanyCount(driver);
		
		for(int i=1; i <= shipCoCtr; i++){
			ShippingCompany = item.getShippingCompanybyPosition(driver, i);
			ShippingCost = item.getShippingCostybyPosition(driver, i);
			EstimatedDeliveryTime = item.getEstimatedDeliveryTimebyPosition(driver, i);
			
			TableContainer.insertIntoCellValue(excelFileDir, rowIndex,  "ShippingProviderName" + i, ShippingCompany);
			TableContainer.insertIntoCellValue(excelFileDir, rowIndex,  "ShippingCost" + i, ShippingCost);
			TableContainer.insertIntoCellValue(excelFileDir, rowIndex,  "ShippingEstTime" + i, EstimatedDeliveryTime);
			
		}
		
	}

	public static void extractProductDataFromHTMLSourceItemPage(WebDriver driver, int itemCtr, String itemUrl, String outputReport) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {

			String xmlString = driver.getPageSource();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        Document document = builder.parse(new InputSource(new StringReader(xmlString)));
	        document.getDocumentElement().normalize();
	        
	        ArrayList<String> text = XMLElements.getElementTextContentbyXPath(document, "//h1[@itemprop]");
	        StatusLog.log("Element Value: " + text.get(0));
	        
	}
	
	
}
