package com.scrape.includes;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;
import com.grund.utility.TakeScreenShot;
import com.grund.utility.WindowSwitch;

public class Quit {
	
	static Properties sys = System.getProperties();
	
	public static void endTest(WebDriver driver, String reportDIR, String cellValue, String colName, String result) throws IOException {
		
		StatusLog.log("Ending test run...");
		TableContainer.insertIntoCellbyCellValueColValue(reportDIR, cellValue, colName, result);
		
		try {
			TakeScreenShot.CurrentPage(driver, "Last Page Results");
			
		} catch (IOException e) {
			StatusLog.log("[SCREENSHOT] Error on getting screenshot.");
		}
		
		//close alert if display in order to quit chrome
		try {
			WindowSwitch.alertAccept(driver, "3");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			StatusLog.log("No Alert is present.");
		}
		
		for (String handle : driver.getWindowHandles()) {
		    driver.switchTo().window(handle);
		    driver.close();
		}
		
		 driver.quit();
		
	}

	public static void closeBrowser(WebDriver driver) {
		for (String handle : driver.getWindowHandles()) {
		    driver.switchTo().window(handle);
		    driver.close();
		}
		
	}
	
}
