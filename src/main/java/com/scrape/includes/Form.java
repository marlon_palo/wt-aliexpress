package com.scrape.includes;

import org.openqa.selenium.WebDriver;
import com.grund.form.SetElementAttribute;
import com.grund.request.ClickElement;
import com.grund.utility.StatusLog;
import com.grund.utility.Wait;
import com.grund.verify.verifyXPath;

public class Form {

	public static void selectSize(WebDriver driver, String string) throws Exception {
		SetElementAttribute.byXPath(driver, "//div[@role='option' and @data-value='5.5']", "class", "");
		
		ClickElement.byXPath(driver, "(//div[@role='option' and @data-value='5.5']//content) [position()=2]");
		Wait.sleep("6");
		int xpathctr = verifyXPath.count(driver, "//div[@role='option' and @data-value='5.5' and contains(@class,'isSelected')]");
		
		if(xpathctr > 0){
			StatusLog.log("XPATH FOUND");
		} else {
			StatusLog.log("XPATH NOT FOUND");
		}
		
		
	}

}
