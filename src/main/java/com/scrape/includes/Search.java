package com.scrape.includes;

import org.openqa.selenium.WebDriver;

import com.grund.engine.Config;
import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;
import com.grund.utility.Wait;
import com.grund.verify.verifyXPath;
import com.scrape.homepage.homepage;
import com.scrape.place.place;
import com.scrape.search.Srch;


public class Search {
	
	public static int lastPageItemCount;
	public static int currentPageNumber;

	public static void keyword(WebDriver driver, String keyword) throws Exception {
		
		Srch.setKeywordField(driver, keyword);
		Srch.clickSearchBtn(driver);
		Wait.sleep("3");
		
	}

	public static int getItemListCount(WebDriver driver) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static void storeAllURLinPageResults(WebDriver driver, String excelFileDir) throws Exception {
		
		int resultCtr = 0;
				
		String CompanyName;
		int RowLastNum;
		boolean flag = true;
		
		
	
		while (flag){
			
			//get the last row count first
			RowLastNum = TableContainer.getRowCountinExcelFile(excelFileDir);
			resultCtr = Srch.getResultsCountonPage(driver);
			StatusLog.log("Results: " + resultCtr);
			for(int n = 1; n <= resultCtr; n++){
				
				try {
					
					CompanyName = Srch.getResultRowTextbyPosition(Config.driver, n);
					Srch.clickResultRowbyPosition(driver, n);
					String URl = PlaceDetail.getWebsiteText(driver);
					
					//insert after the last row
					int rowIndex = RowLastNum + n;

					TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "companyName",CompanyName);
					TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "url",URl);
					
					place.clickBacktoResultsLink(driver);
				
				} catch (Throwable e){
					
				} //try
				
			} //end for
			
			
			if(Srch.isNextPageEnabled(driver)){
				Srch.clickNextPage(driver);
			} else {
				flag = false;
				break;
			} //if
			
		} //while
		
		
		
		
		
	}

	public static int getItemListCountOnPage(WebDriver driver) {
		
		lastPageItemCount = Srch.getItemListCountOnPage(driver);
		
		return Srch.getItemListCountOnPage(driver);
	}

	public static void clickGroupSimilarProduct(WebDriver driver) {
		Srch.clickGroupSimilarProduct(driver);
		
	}

	public static boolean isPageNumActive(WebDriver driver, int pageCtr) {
		
		return Srch.isPageNumActive(driver, pageCtr);
	}

	public static void clickPagebyNumber(WebDriver driver, int pageCtr) throws Exception {
		Srch.clickPagebyNumber(driver, pageCtr);
		
		currentPageNumber = pageCtr;
		
		
	}

}
