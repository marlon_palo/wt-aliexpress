package com.scrape.includes;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.grund.engine.Config;
import com.grund.utility.StatusLog;
import com.grund.utility.Wait;
import com.scrape.homepage.homepage;
import com.scrape.login.login;

public class Homepage {
	
	public static void setupConfig(String host, String browser) throws Exception {
		Properties pr = Config.properties("config.properties"); 
		String driverstr = null;
		
		try {
			driverstr = Config.driver.toString();
		} catch (NullPointerException e) {
			driverstr = "";
		}
		
		if(!driverstr.isEmpty()){
			StatusLog.log("Closing " + driverstr);
			Quit.closeBrowser(Config.driver);

			Config.driver.quit();
		}
	
		Config.browser = browser;
		Config.setup(host,pr.getProperty("TIMEOUT"));
		closeCouponModalClose(Config.driver);
		
	}

	public static void closeCouponModalClose(WebDriver driver) throws InterruptedException {
		homepage.clickCouponModalClose(driver);
		Wait.sleep("3");
	
	}

	public static void login(WebDriver driver, String email, String password) throws Exception {
		homepage.clickSignIn(driver);
		login.setAccountField(driver, email);
		login.setPasswordField(driver, password);
		login.clickSignIn(driver);
		
		
	}
}
