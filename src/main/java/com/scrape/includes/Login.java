package com.scrape.includes;

import java.io.FileReader;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.grund.form.SetInputField;
import com.grund.request.ClickElement;


public class Login {

	public static Properties properties;

	public static void gmailAccount(WebDriver driver, String email, String password) throws Exception {

		FileReader reader = new FileReader("upwork.properties");
		properties = new Properties();
		properties.load(reader);
		
		//ClickElement.byXPath(driver, properties.getProperty("SIGNIN_LINK_XPATH"));
		SetInputField.byXPath(driver, properties.getProperty("EMAIL_INPUT_XPATH"), email);
		ClickElement.byXPath(driver, properties.getProperty("NEXT_BTN_XPATH"));
		
		SetInputField.byXPath(driver, properties.getProperty("PASSWORD_INPUT_XPATH"), password);
		ClickElement.byXPath(driver, properties.getProperty("SIGNIN_BTN_XPATH"));
		
	}
	

}
