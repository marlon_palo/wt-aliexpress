package com.scrape.search;

import org.openqa.selenium.WebDriver;

import com.grund.engine.Config;
import com.grund.form.SetInputField;
import com.grund.request.ClickElement;
import com.grund.verify.verifyXPath;

public class Srch {

	private static final String INPUT_SEARCH_XPATH = "//input[@id='search-key']";
	private static final String BTN_SEARCH_XPATH = "//input[@class='search-button']";
	private static final String LINK_RESULTS_XPATH = "//div[@class='section-result-text-content']//h3/span";
	private static final String LINK_NEXTPAGE_XPATH = "//button[@id='section-pagination-button-next' and not(@disabled)]";
	private static final String DIV_SEARCHITEM_XPATH = "//div[@class='item']";
	private static final String LINK_GROUPSIMILARPROD_XPATH = "//a[@id='group-similar-products']";
	private static final String DIV_PAGINATIONGRP_XPATH = "//div[@class='ui-pagination-navi util-left']";

	public static void setKeywordField(WebDriver driver, String text) {
		SetInputField.byXPath(driver, INPUT_SEARCH_XPATH, text, "ALI EXPRESS: Enter Search keyword.");
		
	}

	public static void clickSearchBtn(WebDriver driver) {
		ClickElement.byXPath(driver, BTN_SEARCH_XPATH, "ALI EXPRESS: Click Search Button.");
		
	}

	public static int getResultsCountonPage(WebDriver driver) {
		
		try {
			return verifyXPath.count(Config.driver, LINK_RESULTS_XPATH,"3","3", "GOOGE MAP: Get the count of results row in current page.");
		} catch (Exception e) {
			return 0;
		}
		
	}

	public static void clickResultRowbyPosition(WebDriver driver, int pos) throws Exception {
		ClickElement.byXPath(driver,"(" + DIV_SEARCHITEM_XPATH + ") [position()=" + pos + "]", "ALI EXPRESS: Click the results in the row position "  + pos);
		
	}

	public static String getResultRowTextbyPosition(WebDriver driver, int pos) {
		try {
			return verifyXPath.getText(driver, "(" + LINK_RESULTS_XPATH + ") [position()=" + pos + "]", "ALI EXPRESS: Get the header title of the result row in position " + pos);
		} catch (Exception e) {
			return "";
		}
		
	}

	public static void clickNextPage(WebDriver driver) {
		try {
			ClickElement.byXPath(driver, LINK_NEXTPAGE_XPATH);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static boolean isNextPageEnabled(WebDriver driver) {
		
		int xpathctr;
		
		try {
			xpathctr = verifyXPath.count(driver, LINK_NEXTPAGE_XPATH,"3","2","loop");
			
			if(xpathctr > 0){
				return true;
			} else {
				return false;
			}
			
		} catch (Exception e) {
			return false;
		}
		
		
	}

	public static int getItemListCountOnPage(WebDriver driver) {
		
		try {
			return verifyXPath.count(driver, DIV_SEARCHITEM_XPATH);
		} catch (Exception e) {
			return 0;
		}
	}

	public static String getResultItemHrefbyPosition(WebDriver driver, int pos) {
		
		return verifyXPath.getAttributeValue(driver, "((" + DIV_SEARCHITEM_XPATH + ") [position()=" + pos + "]//a) [position()=1]", "href", "ALI EXPRESS - SEARCH: Get the href value of item in position: " + pos);
	}

	public static void clickGroupSimilarProduct(WebDriver driver) {
		ClickElement.byXPath(driver, LINK_GROUPSIMILARPROD_XPATH, "ALI EXRESS - SEARCH: Click Group Similar Product.");
		
	}

	public static boolean isPageNumActive(WebDriver driver, int num) {
		int xpathctr;
		try {
			xpathctr = verifyXPath.count(driver, "" + DIV_PAGINATIONGRP_XPATH + "//a[@href and text()=" + num + "]");
			
			if(xpathctr > 0){
				return true;
			} else {
				return false;
			}
			
		} catch (Exception e) {
			return false;
		}
		
		
		
	}

	public static void clickPagebyNumber(WebDriver driver, int num) throws Exception {
		ClickElement.byXPath(driver, "" + DIV_PAGINATIONGRP_XPATH + "//a[@href and text()=" + num + "]", "ALI EXPRESS - SEARCH: Click page number " + num);
		
	}


}
