package com.scrape.homepage;

import org.openqa.selenium.WebDriver;

import com.grund.request.ClickElement;

public class homepage {

	private static final String LINK_COUPONMODALCLOSE_XPATH = "//div[@class='ui-window-content']//a[@class='close-layer']";
	private static final String LINK_SIGNIN_XPATH = "(//a[@data-role='sign-link']) [position()=1]";

	public static void clickCouponModalClose(WebDriver driver) {
		ClickElement.byXPath(driver, LINK_COUPONMODALCLOSE_XPATH, "ALIEXPRESS: Close Modal");
		
	}

	public static void clickSignIn(WebDriver driver) {
		ClickElement.byXPath(driver, LINK_SIGNIN_XPATH, "ALI EXPRESS: CLick Sign in.");
		
	}

}
