package com.scrape.place;

import org.openqa.selenium.WebDriver;

import com.grund.engine.Config;
import com.grund.request.ClickElement;
import com.grund.verify.verifyXPath;

public class place {

	private static final String LINK_WEBSITEINFO_XPATH = "//div//span[contains(@aria-label,'Website')]/..//a[@data-attribution-url]";
	private static final String LINK_BACKTORESULTS_XPATH = "//span[text()='Back to results']";

	public static String getWebsiteText(WebDriver driver) {
		
		int xpathctr;
		
		try {
			xpathctr = verifyXPath.count(driver, LINK_WEBSITEINFO_XPATH);
			
			if(xpathctr > 0){
				return verifyXPath.getText(driver, LINK_WEBSITEINFO_XPATH, "GOOGLE MAP - PLACE: Get the website text.");
			} else {
				return "";
			}
			
		} catch (Exception e) {
			return "";
		}
	}

	public static void clickBacktoResultsLink(WebDriver driver) throws Exception {
		ClickElement.byXPath(Config.driver, LINK_BACKTORESULTS_XPATH);
		
	}

}
