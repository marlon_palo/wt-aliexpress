package com.scrape;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.grund.engine.Config;
import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;
import com.scrape.includes.Homepage;
import com.scrape.includes.PDP;
import com.scrape.includes.Quit;
import com.scrape.includes.Search;


public class SearchScrapeAliExpress {

	private static String reportDIR;
	public String env;
	public String keyword;
	public static String title;
	public Boolean NextPageActive;
	public boolean testStatus;
	public String isGroupSimilarProduct;
	static Properties sys = System.getProperties();
	
	
	@Test
	public void Search_Scrape_AliExpress() throws Exception{

		try 
		{
			
			reportDIR = sys.getProperty("tableReportContainer");
			String outputReport = sys.getProperty("outputReportContainer");
			Properties pr = Config.properties("config.properties"); 
			String now = new SimpleDateFormat("MM/dd/YYYY").format(new Date());
			String email = pr.getProperty("default.email");
			String password = pr.getProperty("default.password");;
			//Get the data in the excel and quick add the skus
			int rowCtr = TableContainer.getRowCount();
			
			for(int i = 1; i <= rowCtr; i++){
				keyword = TableContainer.getCellValue(i, "keyword");
				isGroupSimilarProduct = TableContainer.getCellValue(i, "isGroupSimilarProduct");
				
				title = TableContainer.getCellValue(i, "title");
				
				Homepage.setupConfig(sys.getProperty("host"),sys.getProperty("browser"));
				//Homepage.login(Config.driver, email, password);
				Search.keyword(Config.driver,keyword);
				if(isGroupSimilarProduct.equalsIgnoreCase("true")){
					Search.clickGroupSimilarProduct(Config.driver);
				}
				
				//Navigate in Search Pages
				boolean flag = true;
				int pageCtr = 1;
				int lastPageItemCtr = 0;
				//int currentPageItemCtr = Search.getItemListCountOnPage(Config.driver);
				
	
				while (flag){
					
					PDP.extractProductDataFromSearchCurrentPage(Config.driver, keyword, lastPageItemCtr, outputReport);
					
					
					lastPageItemCtr = lastPageItemCtr + Search.lastPageItemCount;
					
					pageCtr = pageCtr + 1;
					
					if(Search.isPageNumActive(Config.driver, pageCtr)){
						Search.clickPagebyNumber(Config.driver, pageCtr);
						
						
						StatusLog.log("CURRENT PAGE NUMBER IS " + pageCtr);
						
					} else {
						StatusLog.log("LAST PAGE REACHED: " + (pageCtr - 1));
						flag = false;
					} //end if
					
					
				} // end while
				
				//PDP.extractProductDataFromSearchCurrentPage(Config.driver, keyword, outputReport);
				

			} //end for	

			//Overall Test Result
			Assert.assertTrue(StatusLog.errMsg, StatusLog.tcStatus);
			
		}
		
			catch (Exception e)
			{
				Assert.fail(e.getMessage());
				
			}
		
	}
	
	@AfterClass
	public static void quit() throws IOException{
		Quit.endTest(Config.driver,reportDIR, title, "Status", StatusLog.tcResult);
		
	}
	
	
}
