package com.scrape;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.grund.engine.Config;
import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;
import com.scrape.includes.Homepage;
import com.scrape.includes.PDP;
import com.scrape.includes.Quit;
import com.scrape.includes.Search;


public class ItemInvokeURLScrapeAliExpress {

	private static String reportDIR;
	public String env;
	public static String tctitle = "Item Scrape Ali Express";
	public Boolean NextPageActive;
	public boolean testStatus;
	static Properties sys = System.getProperties();
	
	
	@Test
	public void Item_Scrape_AliExpress() throws Exception{

		try 
		{
			
			reportDIR = sys.getProperty("tableReportContainer");
			String outputReport = sys.getProperty("outputReportContainer");
			Properties pr = Config.properties("config.properties"); 
			String now = new SimpleDateFormat("MM/dd/YYYY").format(new Date());
			String email = pr.getProperty("default.email");
			String password = pr.getProperty("default.password");;
			//Get the data in the excel and quick add the skus
			int rowCtr = TableContainer.getRowCount();
			
			for(int i = 1; i <= rowCtr; i++){
				String itemUrl = TableContainer.getCellValue(i, "itemUrl");
				String title = TableContainer.getCellValue(i, "title");
				
				Homepage.setupConfig(itemUrl,sys.getProperty("browser"));
				//Homepage.login(Config.driver, email, password);
				
				//PDP PAGE
				PDP.extractProductDataFromCurrentItemPage(Config.driver, i, itemUrl, outputReport);
				

			} //end for	

			//Overall Test Result
			Assert.assertTrue(StatusLog.errMsg, StatusLog.tcStatus);
			
		}
		
			catch (Exception e)
			{
				Assert.fail(e.getMessage());
				
			}
		
	}
	
	@AfterClass
	public static void quit() throws IOException{
		Quit.endTest(Config.driver,reportDIR, tctitle, "Status", StatusLog.tcResult);
		
	}
	
	
}
