package com.scrape;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.grund.engine.Config;
import com.grund.request.ClickElement;
import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;
import com.grund.utility.TakeScreenShot;
import com.grund.verify.verifyXPath;
import com.scrape.includes.Homepage;
import com.scrape.includes.Quit;
import com.scrape.includes.Search;


public class SearchGoogleMap {

	private static String reportDIR;
	public String env;
	public String keyword;
	public static String title;
	public Boolean NextPageActive;
	public boolean testStatus;
	static Properties sys = System.getProperties();
	
	
	@Test
	public void Search_GoogleMap() throws Exception{

		try 
		{
			
			reportDIR = sys.getProperty("tableReportContainer");
			String outputReport = sys.getProperty("outputReportContainer");
			Properties pr = Config.properties("config.properties"); 
			String now = new SimpleDateFormat("MM/dd/YYYY").format(new Date());
			
			//Get the data in the excel and quick add the skus
			int rowCtr = TableContainer.getRowCount();
			
			for(int i = 1; i <= rowCtr; i++){
				keyword = TableContainer.getCellValue(i, "keyword");
				title = TableContainer.getCellValue(i, "title");
				
				Homepage.setupConfig(sys.getProperty("host"),sys.getProperty("browser"));
				
				Search.keyword(Config.driver,keyword);
				Search.storeAllURLinPageResults(Config.driver, outputReport);
				

			} //end for	

			//Overall Test Result
			Assert.assertTrue(StatusLog.errMsg, StatusLog.tcStatus);
			
		}
		
			catch (Exception e)
			{
				Assert.fail(e.getMessage());
				
			}
		
	}
	
	@AfterClass
	public static void quit() throws IOException{
		Quit.endTest(Config.driver,reportDIR, title, "Status", StatusLog.tcResult);
		
	}
	
	
}
