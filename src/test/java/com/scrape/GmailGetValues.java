package com.scrape;


import java.io.IOException;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.grund.engine.Config;
import com.grund.request.ClickElement;
import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;
import com.grund.utility.TakeScreenShot;
import com.grund.verify.verifyXPath;
import com.scrape.includes.Login;


public class GmailGetValues {

	public String env;
	public String email;
	public String password;
	public String title;
	public Boolean NextPageActive;
	public String urlText;
	
	public boolean testStatus;
	
	
	static Properties sys = System.getProperties();
	
	
	@Test
	public void Gmail_Get_Values() throws Exception{

		try 
		{
			
			Config.setup(sys.getProperty("host"));
			env = sys.getProperty("upworkHost");
			Properties pr = Config.properties("upwork.properties"); //create a method for the pcm.properies
			
			
			//Long wait = Long.parseLong(pr.getProperty("WAIT_SEC"));
			//Get the data in the excel and quick add the skus
			int rowCtr = TableContainer.getRowCount();
			
			for(int i = 1; i <= rowCtr; i++){
	
				Config.driver.navigate().to(sys.getProperty("host"));
				
				email = TableContainer.getCellValue(i, "email");
				password = TableContainer.getCellValue(i, "password");
				
				title = TableContainer.getCellValue(i, "title");
				
				Login.gmailAccount(Config.driver,email,password);
				
				//CLICK THE EMAIL SUBJECT
				ClickElement.byXPath(Config.driver, pr.getProperty("EMAIL_TITLE_LINK_XPATH"));
				int linkCt = verifyXPath.count(Config.driver, pr.getProperty("BODY_GETLINK_XPATH"));
				int RowLastNum;
				
				RowLastNum = TableContainer.getRowCountinExcelFile(sys.getProperty("tableOutPutContainer"));
				
				for (int b = 1; b <= linkCt; b++){
					urlText = verifyXPath.getText(Config.driver, "(" + pr.getProperty("BODY_GETLINK_XPATH") + ") [position()=" + b + "]");
					int rowIndex = RowLastNum + b;

					TableContainer.insertIntoCellValue(sys.getProperty("tableOutPutContainer"), rowIndex, "url", urlText);
				}
				
				
			} //end for	

			//Overall Test Result
			Assert.assertTrue(StatusLog.errMsg, StatusLog.tcStatus);
			
		}
		
			catch (Exception e)
			{
				Assert.fail(e.getMessage());
				
			}
		
	}
	
	@AfterClass
	public static void quit() throws IOException{
		TakeScreenShot.CurrentPage(Config.driver, "Last Page Test Result");
		Config.driver.close();
		Config.driver.quit();
	}
	
	
}
